<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Doc</title>
    <link rel="stylesheet" href="day03.css">
</head>
<body>
    <fieldset>
        <form action="form">
            <div>
                <div class="apart">
                    <div class= "style">
                        <div class="name">
                            <label>
                                Họ và tên
                            </label>
                        </div>
                        <input type="text"  class="text" id="user_name" name="user_name">
                    </div>
                </div>
                <div class="apart">
                    <div class= "style">
                        <div class="name">
                            <label>
                                Giới tính
                            </label>
                        </div>
                        <div style="display: flex;align-items: center;">
                            <input type="radio" class="gender" value="0" name="gender">
                            <label for="">Nam</label>
                            <input type="radio" class="gender" value="1" name="gender">
                            <label for="">Nữ</label>
                        </div>
                    </div>
                </div>
                <div class="apart">
                    <div class= "style" >
                        <div class="name">
                            <label>
                                Phân khoa
                            </label>
                        </div>
                        <select class="depart">
                            <option value="">Trống</option>
                            <option value="MAT">Khoa học máy tính</option>
                            <option value="KDL">Khoa học vật liệu</option>
                        </select>
                    </div>
                </div>
                <button class="button" type="button" >Đăng ký</button>
            </div>
        </form>
    </fieldset>
</body>

</html>

